package br.com.itau.Pedido.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name="pagamento")
public interface PagamentoClient {

	@GetMapping
	public boolean buscarStatusPagamento();

}
