package br.com.itau.Pedido.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.Pedido.services.PedidoService;
import br.com.itau.Pedido.viewobjects.PedidoCursoCliente;

@RestController
@RequestMapping("/pedido")
public class PedidoController {
	
	@Autowired
	PedidoService pedidoService;

	@PostMapping
	public ResponseEntity pedidoCliente(@RequestBody PedidoCursoCliente pedidoCliente) {
		boolean pedido = pedidoService.verificardisponibilidadePagamento(pedidoCliente);
		
		if (pedido) {
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.status(400).build();
	}
}
