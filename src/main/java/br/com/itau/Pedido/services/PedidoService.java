package br.com.itau.Pedido.services;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.itau.Pedido.clients.CursoClient;
import br.com.itau.Pedido.clients.PagamentoClient;
import br.com.itau.Pedido.models.Pedido;
import br.com.itau.Pedido.repositories.PedidoRepository;
import br.com.itau.Pedido.viewobjects.Curso;
import br.com.itau.Pedido.viewobjects.PedidoCursoCliente;

@Service
public class PedidoService {
	
	private boolean statusPagamento;
	private boolean statusDisponibilidadeCurso;

	@Autowired
	PedidoRepository pedidoRepository;

	@Autowired
	CursoClient cursoClient;
	
	@Autowired
	PagamentoClient pagamentoClient;
	
	Curso curso = new Curso();

	public boolean verificardisponibilidadePagamento(PedidoCursoCliente pedidoCliente) {
		statusDisponibilidadeCurso = obterDadosDoCurso(pedidoCliente.getNomeCurso());
		statusPagamento = obterDadosDoPagamento();
		
		if (statusDisponibilidadeCurso && statusPagamento) {
			if (gravarPedido(pedidoCliente)) {
				return true;
			}
			return false;
		}
		return false;
	}
	
	private boolean gravarPedido(PedidoCursoCliente pedidoCliente) {
		Pedido pedido = new Pedido();

		pedido.setPrecoPago(curso.getPreco());
		pedido.setNomeCliente(pedidoCliente.getNomeCliente());
		pedido.setNomeCurso(pedidoCliente.getNomeCurso());
		pedido.setStatusPagamento(statusPagamento);
		pedido.setDataEfetivacaoPagamento(LocalDateTime.now());
		
		pedidoRepository.save(pedido);
		return true;
	}

	private boolean obterDadosDoCurso(String nomeCurso){
		
		Curso curso1 = cursoClient.buscarCursoPorNome(nomeCurso);
		
		if (curso1.isDisponivel()){
			curso.setPreco(curso1.getPreco());
		}
		return curso1.isDisponivel();
	}
	
	private boolean obterDadosDoPagamento(){
		return pagamentoClient.buscarStatusPagamento();
	}
}